<form class="" action="<?php echo site_url(); ?>/clientes/editar" method="post">
  <div class="form-group">
    <input type="hidden" class="form-control" id="id_cli" name="id_cli">
<label for="email">Cedula del cliente:</label>
<input type="text" class="form-control" id="cedula_cli" name="cedula_cli">
</div>
<div class="form-group">
<label for="pwd">Nombre:</label>
<input type="text" class="form-control" id="nombre_cli" name="nombre_cli">
</div>
<div class="form-group">
<label for="pwd">Apellido:</label>
<input type="text" class="form-control" id="apellido_cli" name="apellido_cli">
</div>
<div class="form-group">
<label for="pwd">Genero:</label>
<select class="form-control" name="genero" id="genero">
  <option value="">---Seleccione---</option>
  <option value="MASCULINO">MASCULINO</option>
  <option value="FEMENINO">FEMENINO</option>
</select>
</div>
<div class="form-group">
<label for="pwd">Fecha de nacimiento:</label>
<input type="date" class="form-control" id="fecha_cli" name="fecha_cli">
</div>

<br>
<button type="submit" class="btn btn-default">GUARDAR</button>
</form>
