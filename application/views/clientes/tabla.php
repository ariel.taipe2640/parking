<?php if($listadoClientes): ?>
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">#</th>
        <th class="text-center">CEDULA</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">GENERO</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoClientes->result() as $clienteTemporal):?>
        <tr>
          <td class="text-center"><?php echo $clienteTemporal->id_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->cedula_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->nombre_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->apellido_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->genero ?></td>
          <td class="text-center"><?php echo $clienteTemporal->fecha_cli ?></td>
          <td class="text-center">
            <a href="#">
              <i class="glyphicon glyphicon-pencil" title="editar"></i>
            </a>
            <a  href="<?php echo site_url(); ?>/clientes/eliminarClientes/<?php echo $clienteTemporal->id_cli ?>"
               onclick="confirmation(event)">

              <i class="glyphicon glyphicon-trash" title="eliminar"></i>

          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>

  </table>
<?php else: ?>
  <div class="alet alert-danger">
    no se enuentra datos
  </div>
<?php endif; ?>
