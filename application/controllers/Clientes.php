<?php
  /**
   *
   */
  class Clientes extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('cliente');

    }

    public function gestionClientes(){
      $this->load->view('encabezado');
      $this->load->view('clientes/gestionClientes');

    }

    public function insertarCliente(){
      $datos=array(
        "cedula_cli"=>$this->input->post("cedula_cli"),
        "nombre_cli"=>$this->input->post("nombre_cli"),
        "apellido_cli"=>$this->input->post("apellido_cli"),
        "genero"=>$this->input->post("genero"),
        "fecha_cli"=>$this->input->post("fecha_cli")

      );
      if ($this->cliente->insertar($datos)) {
        redirect("clientes/gestionClientes");
      }else {
        redirect("clientes/gestionClientes");
      }
    }

    public function listado(){
      $data["listadoClientes"]=$this->cliente->obtenerTodos();
      $this->load->view("clientes/tabla",$data);
    }

    public function eliminarClientes($id){
      if ($this->cliente->eliminarPorId($id)) {
        redirect('clientes/gestionClientes');
      }else{
        echo 'error';
      }
    }

    public function editar($id){
      $data['clientesEditar']=$this->cliente->obtenerPorId($id);
      $this->load->view('encabezado');
      $this->load->view('clientes/editar',$data);

    }

  }

 ?>
